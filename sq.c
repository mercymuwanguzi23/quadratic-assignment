#include <stdio.h>
#include <math.h>
int calculate_func(int a, int b, int c){
    double discriminant  = b*b - 4*a*c;
    if(discriminant  >= 0){
        double x1 = (-b + sqrt(discriminant ))/(2*a);
        double x2 = (-b - sqrt(discriminant ))/(2*a);
        printf("x1= %.2f\n",x1);
        printf("x2=%.2f\n",x2);
        return 0; 
    }else{
        printf("the roots are complex\n");
        return 1; 
    }
}
int main(){
    int a, b, c;
    printf("enter the value a \n");
    scanf(" %d",&a);
    if(a==0){
        printf("a can't be zero\n");
        return 1;
    }
    printf("enter the value b \n");
    scanf(" %d",&b);
    printf("enter the value c \n");
    scanf(" %d",&c);
    return calculate_func(a,b,c);
}
